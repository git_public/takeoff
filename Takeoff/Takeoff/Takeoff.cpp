#include<iostream>
#include<thread>
#include<chrono>

int main()
{
	std::cout << "Time until takeoff: ";
	for(int i = 12; i >= 0; i--)
	{
		std::cout << i;
		std::this_thread::sleep_for(std::chrono::duration<int, std::milli>(1000));
		if(i > 0)
		{
			std::cout << "\b \b";
		}
		if(i > 9)
		{
			std::cout << "\b";
		}
	}

	for(size_t i = 5; i > 0; i--)
	{
		std::this_thread::sleep_for(std::chrono::duration<int, std::milli>(500));
		std::cout << '\r' << "                     " << '\r' << std::flush;
		std::this_thread::sleep_for(std::chrono::duration<int, std::milli>(500));
		std::cout << "Takeoff" << '\r' << std::flush;
	}
}